const canvas = document.querySelector('.bpsCanvas');
const width = canvas.width = window.innerWidth;
const height = canvas.height = window.innerHeight;

const x = width/2;
const y = height/2;

const ctx = canvas.getContext('2d');

// Base rect
ctx.fillStyle = 'rgb(0, 0, 0)';
ctx.fillRect(0, 0, width, height);

// Red rect
ctx.fillStyle = 'rgb(255, 0, 0)';
ctx.fillRect(x + 50, y + 50, 100, 150);

// Green rect
ctx.fillStyle = 'rgb(0, 255, 0)';
ctx.fillRect(x + 75, y + 75, 100, 100);


// Alpha rect
ctx.fillStyle = 'rgba(255, 0, 255, 0.75)';
ctx.fillRect(x + 25, y + 100, 175, 50);

ctx.strokeStyle = 'rgb(255, 255, 255)';
ctx.strokeRect(x + 25, y + 25, 175, 200);
ctx.lineWidth = 5;

